Développeur : Loïc Olivier et Mélanie Engrand

Installation du projet : ce projet se clone comme n'importe quel projet git, il n'y a pas d'installation supplémentaire que vous aurez besoin de vous occupez.
Rappel de l'utilisation :
1ère étape : Ouvrir le terminal
2ème étape : Se positionner dans le fichier où vous souhaitez cloner le repository
3ème étape : Copier la clé http depuis le repôt distant
4ème étape : Effectuer un git clone suivis de la clé http
5ème étape : Enjoy !

Description du projet : nous avons décidé de créer une interface numérique pour le jeu affricain appelé Awalé. Nous avons réalisé ce projet dans le cadre de notre formation pour Simplon. Et il comporte uniquement du html, du css et du javascript.


Etat du projet : ce projet est en cour de construction, cependant vous aurez déjà accès à la page principale du jeu avec les boutons cliquable 'new game' et 'computeur'. Le reste des fonctionnalités n'est pas encore accessible.

Langage utilisés : html, css & javascript pure. 
